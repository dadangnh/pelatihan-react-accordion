import React from 'react';
// use Accordion Class
// import AccordionClass from './AccordionClass';
// use Accordion function
// import Accordion from './Accordion';
// use Search
import Search from './Search';

const items = [
    {
        title: 'What is React?',
        content: 'React is a front end library',
    },
    {
        title: 'Why use React?',
        content: 'Easy'
    },
    {
        title: 'How do we use React?',
        content: 'Just install it'
    }
]

const App = () => {
    return(
        <div>
            {/*<Accordion items={items} />*/}
            <Search />
        </div>
    )
}

export default App;
