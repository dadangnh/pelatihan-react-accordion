import React from 'react';

class AccordionClass extends React.Component {

    state = {
        activeIndex: 0
    }

    onTitleClick = (index) => {
        this.setState({ activeIndex: index });
    }

    render() {
        const renderedItems = this.props.items.map((item, index) => {
            const active = index === this.state.activeIndex ? 'active' : '';

            return (
                <div key={index}>
                    <div className={`title ${active}`} onClick={() => this.onTitleClick(index)}>
                        <i className="dropdown icon" />
                        {item.title}
                    </div>
                    <div className={`content ${active}`}>
                        {item.content}
                    </div>
                </div>
            );
        })
        return (
            <div className="ui styled accordion">
                {renderedItems}
            </div>
        )
    }
}

export default AccordionClass;
